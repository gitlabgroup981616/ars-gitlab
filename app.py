# app.py
# print("Hello from HelloCloud-v1")



# app.py
from http.server import BaseHTTPRequestHandler, HTTPServer

MESSAGE = "Hello from Cohort6!"

class EchoHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(MESSAGE.encode())

def run(server_class=HTTPServer, handler_class=EchoHandler, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting httpd server on port {port}...')
    httpd.serve_forever()

if __name__ == "__main__":
    run()